Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Dim NV, edgesLen As Integer
        Dim edges(), answer() As Integer
        NV = CInt(Console.ReadLine())
        edgesLen = CInt(Console.ReadLine())
        ReDim edges(edgesLen - 1)
        For i As Integer = 0 To edgesLen - 1
            edges(i) = Console.ReadLine()
        Next i
        Dim graphDrawing As New GraphDrawing()
        answer = graphDrawing.plot(NV, edges)
        Console.WriteLine(answer.Length)
        For Each point As Integer In answer
            Console.WriteLine(point)
        Next point
        Console.Out.Flush()
    End Sub

End Module