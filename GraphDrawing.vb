Option Explicit On
Imports System
Imports System.Collections.Generic
Imports Tpl = System.Tuple(Of Integer, Integer)
Imports TpD = System.Tuple(Of Integer, Double)

Public Class GraphDrawing

    Private Class Heap(Of V)
        Dim cmp As Comparison(Of V), values() As V, count As Integer
        Sub New(size As Integer, c As Comparison(Of V))
            ReDim values(size): cmp = c
        End Sub
        Sub Push(val As V)
            values(count) = val: count += 1: GoUp(count - 1)
        End SUb
        Function Pop() As V
            Dim t As V = values(0): count -= 1
            values(0) = values(count): GoDown(0): Return t
        End Function
        Sub Beam(size As Integer)
            If count > size Then count = size
        End Sub
        Function IsEmpty() As Boolean
            Return count = 0
        End Function
        Sub Swap(i As Integer, j As Integer)
            Dim t As V = values(i): values(i) = values(j): values(j) = t
        End Sub
        Sub GoUp(i As Integer)
            If i = 0 Then
                GoDown(i): Exit Sub
            End If
            Dim j As Integer = (i - 1) \ 2
            If cmp(values(i), values(j)) < 0 Then
                Swap(i, j): GoUp(j)
            Else
                GoDown(i)
            End If
        End Sub
        Sub GoDown(i As Integer)
            Dim j As Integer = i * 2 + 1
            Dim k As Integer = j + 1
            If j >= count Then Exit SUb
            If k < count Then
                If  cmp(values(j), values(k)) > 0 Then j = k
            End IF
            If cmp(values(i), values(j)) > 0 Then
                Swap(i, j): GoDown(j)
            End If
        End Sub
    End Class

    Public Function plot(NV As Integer, edges As Integer()) As Integer()
        Dim edgeCount = edges.Length \ 3
        Dim dist = Function(a As Integer(), i As Integer, j As Integer) As Double
            Dim dx As Integer = a(j) - a(i)
            Dim dy As Integer = a(j + 1) - a(i + 1)
            Return Math.Sqrt(CDbl(dx * dx + dy * dy))
        End Function
        Dim multi = Function(a As Integer(), i As Integer, j As Integer) As Boolean
            Dim dx As Integer = a(j) - a(i)
            Dim dy As Integer = a(j + 1) - a(i + 1)
            Return dx = 0 And dy = 0
        End Function
        Dim calc = Function(a As Integer()) As Double
            Dim maxRatio As Double = -1.0
            Dim minRatio As Double = -1.0
            For j As Integer = 0 To UBound(edges) Step 3
'                If multi(a, edges(j) * 2, edges(j + 1) * 2) Then
'                    Return 0.0
'                End If
                Dim d As Double = dist(a, edges(j) * 2, edges(j + 1) * 2)
                Dim r As Double = d / CDbl(edges(j + 2))
                If Math.Sign(minRatio) < 0 Or r < minRatio Then
                    minRatio = r
                End If
                If maxRatio < r Then
                    maxRatio = r
                End If
            Next j
            Return minRatio / maxRatio
        End Function
        
        Dim links(NV - 1) As LinkedList(Of TpD)
        For i As Integer = 0 To NV - 1
            links(i) = New LinkedList(Of TpD)()
        Next i
        For i As Integer = 0 To UBound(edges) Step 3
            links(edges(i)).AddFirst(New TpD(edges(i + 1), CDbl(edges(i + 2))))
            links(edges(i + 1)).AddFirst(New TpD(edges(i), CDbl(edges(i + 2))))
        Next i

        Dim lscore = Function(a As Integer(), i As Integer) As Double
            Dim minRatio As Double = -1.0
            Dim maxRatio As Double = -1.0
            For Each pt As TpD In links(i)
                Dim d As Double = dist(a, i * 2, pt.Item1 * 2)
                Dim r As Double = d / pt.Item2
                If Math.Sign(minRatio) < 0 Or r < minRatio Then minRatio = r
                If maxRatio < r Then maxRatio = r
            Next pt
            Return minRatio / maxRatio
        End Function

        Dim rand As New Random(19831983)
        Dim flag(700, 700) As Boolean
        Dim answer(NV * 2 - 1) As Integer
        Dim temp(NV * 2 - 1) As Integer
        Dim cmp As Comparison(Of TpD) = Function(a As TpD, b As TpD) As Integer
            Return a.Item2.CompareTo(b.Item2)
        End Function       
        Dim pq As New Heap(Of TpD)(NV + 1024, cmp)
        Dim last As Integer = -1

        Dim ii As Integer = 0
        For y As Integer = 340 To 381
            For x As Integer = 340 To 381
                flag(x, y) = True
                temp(ii * 2) = x
                temp(ii * 2 + 1) = y
                ii += 1
                If ii >= NV Then Exit For
            Next x
            If ii >= NV Then Exit For
        Next y
        Array.Copy(temp, answer, temp.Length)
        Dim ansScore As Double = calc(answer)

        For i As Integer = 0 To NV - 1
            pq.Push(New TpD(i, lscore(temp, i)))
        Next i

        Dim cos(359), sin(359) As Double
        For i As Integer = 0 To 359
            Dim rd As Double = CDbl(i) * Math.PI / 180#
            cos(i) = Math.Cos(rd)
            sin(i) = Math.Sin(rd)
        Next i

        Dim time0 As Integer = Environment.TickCount + 9500

        Do
            Dim cur As TpD = pq.Pop()
            Dim j As Integer = cur.Item1
            Dim maxScore As Double = cur.Item2
            Dim k As Integer = j * 2
            Dim ox As Integer = temp(k)
            Dim oy As Integer = temp(k + 1)
            Dim x As Integer = ox
            Dim y As Integer = oy
            Dim ch As Boolean = False
            Dim dx As Double = CDbl(ox)
            Dim dy As Double = CDbl(oy)
            For Each t As TpD In links(j)
                Dim u As Integer = t.Item1 * 2
                Dim d As Double = t.Item2 - dist(temp, k, u)
                dx += CDbl(temp(u) - ox) * d / t.Item2
                dy += CDbl(temp(u + 1) - oy) * d / t.Item2
            Next t
            Dim xx As Integer = CInt(Math.Max(0#, Math.Min(700#, Math.Ceiling(dx))))
            Dim yy As Integer = CInt(Math.Max(0#, Math.Min(700#, Math.Ceiling(dy))))
            If Not flag(xx, yy) Then
                temp(k) = xx
                temp(k + 1) = yy
                Dim score As Double = lscore(temp, j)
                If score > maxScore Then
                    x = xx
                    y = yy
                    maxScore = score
                    ch = True
                End IF
            End If
            For i As Integer = 1 To 50
                Dim tx, ty As Integer
                Do
                    tx = rand.Next(0, 701)
                    ty = rand.Next(0, 701)
                Loop While flag(tx, ty)
                temp(k) = tx
                temp(k + 1) = ty
                Dim score As Double = lscore(temp, j)
                If score > maxScore Then
                    x = tx
                    y = ty
                    maxScore = score
                    ch = True
                End IF
            Next i
            Dim lx As Integer = Math.Max(0, ox - 8)
            Dim rx As Integer = Math.Min(700, ox + 8)
            For ty As Integer = Math.Max(0, oy - 8) To Math.Min(700, oy + 8)
                For tx As Integer = lx To rx
                    If flag(tx, ty) Then Continue For
                    temp(k) = tx
                    temp(k + 1) = ty
                    Dim score As Double = lscore(temp, j)
                    If score > maxScore Then
                        x = tx
                        y = ty
                        maxScore = score
                        ch = True
                    End IF
                Next tx
            Next ty
            lx = Math.Max(0, ox - 200)
            rx = Math.Min(700, ox + 200)
            For ty As Integer = Math.Max(0, oy - 200) To Math.Min(700, oy + 200) Step 23
                For tx As Integer = lx To rx Step 23
                    If flag(tx, ty) Then Continue For
                    temp(k) = tx
                    temp(k + 1) = ty
                    Dim score As Double = lscore(temp, j)
                    If score > maxScore Then
                        x = tx
                        y = ty
                        maxScore = score
                        ch = True
                    End IF
                Next tx
            Next ty
            lx = Math.Max(0, ox - 35)
            rx = Math.Min(700, ox + 35)
            Dim ly = Math.Max(0, oy - 35)
            Dim ry = Math.Min(700, oy + 35)
            For i As Integer = 1 To 50
                Dim tx, ty As Integer
                Do
                    tx = rand.Next(lx, rx)
                    ty = rand.Next(ly, ry)
                Loop While flag(tx, ty)
                temp(k) = tx
                temp(k + 1) = ty
                Dim score As Double = lscore(temp, j)
                If score > maxScore Then
                    x = tx
                    y = ty
                    maxScore = score
                    ch = True
                End IF
            Next i
            flag(ox, oy) = False
            flag(x, y) = True        
            temp(k) = x
            temp(k + 1) = y
            If ch And last <> j Then
                pq.Push(New TpD(j, maxScore))
            Else
                Dim score As Double = calc(temp)
                If score > ansScore Then
                    Array.Copy(temp, answer, temp.Length)
                    ansScore = score
                End IF
                For Each r As TpD In links(j)
                    Dim u As Integer = r.Item1 * 2
                    Dim d As Double = dist(temp, u, k) / r.Item2
                    If 0.7 < d And d <= 1.0 Then Continue For
                    x = temp(u)
                    y = temp(u + 1)
                    flag(x, y) = False
                    Dim tx, ty As Integer
                    Do
                        If 0.5 < d And d < 1.5 Then
                            tx = rand.Next(Math.Max(0, x - 100), Math.Min(700, x + 100) + 1)
                            ty = rand.Next(Math.Max(0, y - 100), Math.Min(700, y + 100) + 1)
                        Else
                            Dim dg As Integer = rand.Next(0, 360)
                            tx = CInt(Math.Max(0#, Math.Min(700#, r.Item2 * cos(dg) + CDbl(ox))))
                            ty = CInt(Math.Max(0#, Math.Min(700#, r.Item2 * sin(dg) + CDbl(oy))))
                        End If
                    Loop While flag(tx, ty)
                    flag(tx, ty) = True
                    temp(u) = tx
                    temp(u + 1) = ty
                Next r
                pq.Beam(0)
                For r As Integer = 0 To NV - 1
                    pq.Push(New TpD(r, lscore(temp, r)))
                Next r
            End If
            last = j
        Loop While Environment.TickCount < time0

        If calc(temp) > ansScore Then
            Array.Copy(temp, answer, temp.Length)
        End If

        plot = answer
    End Function
End Class